// Esta funcion cambia el tema cuando cambia el select_tema de index.html
$(document).ready(function(){
  $("#select_tema").change(function(){  // Cuando select_tema cambie se ejecuta la siguiete funcion:
    // Cuando el value de select_tema == azul
    if ($(this).val() == "azul") {
    $("#menu_arriba").css("background-image" , 'linear-gradient(to right bottom, rgb(1, 1, 223), rgb(164, 164, 164))');
    $("#submenu_arriba").css("background-image" , 'linear-gradient(to right bottom, rgb(129,159,247), rgb(1,169,219))');
    }

    // Cuando el value de select_tema == verde
    if ($(this).val() == "verde") {
      $("#menu_arriba").css("background-image", 'linear-gradient(to right bottom, rgb(126,174,91,1), rgb(65,129,18,1))');
      $("#submenu_arriba").css("background-image", 'linear-gradient(to right bottom, rgb(126,174,91,1), rgb(65,129,18,1))');
      $("#submenu_arriba").css("border", 'thin solid green');

    }

    // Cuando el value de select_tema == rojo
    if ($(this).val() == "rojo") {
      $("#menu_arriba").css("background-image", 'linear-gradient(to right bottom, rgb(223,68,48,1), rgb(203,51,37,1), rgb(219,95,81,1), rgb(198,54,47,1))');
      $("#submenu_arriba").css("background-image", 'linear-gradient(to right bottom, rgb(223,68,48,1), rgb(203,51,37,1), rgb(219,95,81,1), rgb(198,54,47,1))');
      $("#submenu_arriba").css("border", 'thin solid red');
    }
 //background: -o-linear-gradient(left, rgba(223,68,48,1) 0%, rgba(203,51,37,1) 25%, rgba(203,46,32,1) 47%, rgba(219,95,81,1) 73%, rgba(198,54,47,1) 100%);

  });

});
